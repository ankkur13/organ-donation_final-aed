/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.NurseRole;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.NGO.FundWorkRequest;
import Business.Organization.DoctorOrganization;
import Business.UserAccount.UserAccount;

import java.awt.CardLayout;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author amandeep
 */
public class DoctorFundRequestsPanel extends javax.swing.JPanel {
    private JPanel userProcessContainer;
    private DoctorOrganization organization;
    private Enterprise enterprise;
    private UserAccount userAccount;
    private EcoSystem business;

    /**
     * Creates new form RequestFundPanel
     */
    public DoctorFundRequestsPanel(JPanel userProcessContainer, UserAccount account, DoctorOrganization organization, Enterprise enterprise, EcoSystem business) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.organization = organization;
        this.enterprise = enterprise;
        this.userAccount = account;
        this.business = business;
        valueLabel.setText(enterprise.getName());
        try{
           populateRequestTable(); 
        }
        catch(Exception e){
            System.out.println("Not There");
        }
        
    }
       public 
   void populateRequestTable(){
       
       DefaultTableModel model = (DefaultTableModel) fundRequestTbl.getModel();
        
        model.setRowCount(0);
        for (FundWorkRequest request : business.getFundRequestQueue().getFundrequestList()){
            Object[] row = new Object[7];
            row[0] = request;
            row[1] = request.getAge();
            row[2] = request.getOrgan();
            row[3] = request.getAmount();
            row[4] = request.getStatus();
            row[5]= request.getSender();
            row[6] = request.getAnnualIncome();
            
            
         
//            if(business.getEnterprise(request.getReceiver())!=enterprise && request.getReceiver()!=null){
//                String receiver = business.getEnterprise(request.getReceiver()).getName()+","+business.getNetwork(userAccount);
//                row[2] = receiver;
//            }
//            else{
//                row[2] = request.getReceiver();
//            }
//            row[3] = request.getAmount();
//           String result = ((FundWorkRequest) request).getTestResult();
//            row[4] = result == null ? "Waiting" : result;
            
            model.addRow(row);
        }
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        fundRequestTbl = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        refreshBtn = new javax.swing.JButton();
        requestfundBtn = new javax.swing.JButton();
        backBtn = new javax.swing.JButton();
        enterpriseLabel = new javax.swing.JLabel();
        valueLabel = new javax.swing.JLabel();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        fundRequestTbl.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Patient Name", "Age", "Organ Transplant", "Amount Required", "Status"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(fundRequestTbl);
        if (fundRequestTbl.getColumnModel().getColumnCount() > 0) {
            fundRequestTbl.getColumnModel().getColumn(0).setResizable(false);
            fundRequestTbl.getColumnModel().getColumn(1).setResizable(false);
            fundRequestTbl.getColumnModel().getColumn(2).setResizable(false);
            fundRequestTbl.getColumnModel().getColumn(3).setResizable(false);
            fundRequestTbl.getColumnModel().getColumn(4).setResizable(false);
        }

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 114, 514, 108));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("Fund Request");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(354, 21, -1, -1));

        refreshBtn.setText("Refresh");
        add(refreshBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 262, -1, -1));

        requestfundBtn.setText("Request Fund");
        requestfundBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                requestfundBtnActionPerformed(evt);
            }
        });
        add(requestfundBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(583, 262, -1, -1));

        backBtn.setText("<< Back");
        backBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backBtnActionPerformed(evt);
            }
        });
        add(backBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 494, -1, -1));

        enterpriseLabel.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        enterpriseLabel.setForeground(new java.awt.Color(51, 51, 255));
        enterpriseLabel.setText("EnterPrise :");
        add(enterpriseLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 59, 139, 30));

        valueLabel.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        valueLabel.setForeground(new java.awt.Color(51, 51, 255));
        valueLabel.setText("<value>");
        add(valueLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(176, 61, 158, 26));
    }// </editor-fold>//GEN-END:initComponents

    private void backBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backBtnActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_backBtnActionPerformed

    private void requestfundBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_requestfundBtnActionPerformed
        // TODO add your handling code here:
        System.out.println("business dp"+business.getFundRequestQueue());
//        System.out.println("business dp q1"+business.getOrganCatalogue());
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
       // userProcessContainer.add("RequestFundPanel", new RequestFundPanel(userProcessContainer, userAccount, enterprise, business));
        layout.next(userProcessContainer);
        
    }//GEN-LAST:event_requestfundBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backBtn;
    private javax.swing.JLabel enterpriseLabel;
    private javax.swing.JTable fundRequestTbl;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton refreshBtn;
    private javax.swing.JButton requestfundBtn;
    private javax.swing.JLabel valueLabel;
    // End of variables declaration//GEN-END:variables
}
