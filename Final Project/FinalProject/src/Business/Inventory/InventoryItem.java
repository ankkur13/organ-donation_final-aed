/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Inventory;

import Business.DiseasesDrugs.Diseases;
import Business.DiseasesDrugs.Vaccination;

/**
 *
 * @author sreer
 */
public class InventoryItem {
    private Vaccination vacination;
    private int quantity;

    public Vaccination getVacination() {
        return vacination;
    }

    public void setVacination(Vaccination vacination) {
        this.vacination = vacination;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
