/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Organization.Organization.DoctorType;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author raunak
 */
public class OrganizationDirectory {

    private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList<>();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }

    public Organization createOrganization(DoctorType type) {
        Organization organization = null;
        if (type.getValue().equals(DoctorType.Doctor.getValue())) {
            organization = new DoctorOrganization();
            organizationList.add(organization);
        } else if (type.getValue().equals(DoctorType.Nurse.getValue())) {
            organization = new NurseOrganization();
            organizationList.add(organization);
        } else if (type.getValue().equals(DoctorType.OrganAdmin.getValue())) {
            organization = new OrganRepositoryOrganization();
            organizationList.add(organization);
        } else if (type.getValue().equals(DoctorType.NGO.getValue())) {
            organization = new NGOOrganization();
            organizationList.add(organization);
        }
        return organization;
    }

//    public void organizationIsExists(DoctorType type) {
//        for (Organization org : organizationList) {
//            if (type.getValue().equals(org.getName())) {
//                JOptionPane.showMessageDialog(null, "Organization already exists");
//            }
//        }
//    }
    
    
    public boolean organizationIsExists(DoctorType type) {
        for (Organization org : organizationList) {
            if (type.getValue().equals(org.getName())) {
                JOptionPane.showMessageDialog(null, "The Organization already exists, Please select a different Organization");
                return false;
            }
        }
        return true;
    }
    
    
    
    
    

    public void removeOrganization(Organization org) {
        organizationList.remove(org);
    }
}
