/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Employee.EmployeeDirectory;
import Business.Role.Role;
import Business.UserAccount.UserAccountDirectory;
import Business.WorkQueue.WorkQueue;
import java.util.ArrayList;
import Business.NGO.FundRequestQueue;
import Business.OrganDonate.DonorWorkQueue;

/**
 *
 * @author raunak
 */
public abstract class Organization {

    private String name;
    private WorkQueue workQueue;
    private EmployeeDirectory employeeDirectory;
    private UserAccountDirectory userAccountDirectory;
    private int organizationID;
    private static int counter=1;
    private FundRequestQueue fundRequestQueue;
     private DonorWorkQueue donorWorkQueue ;
    
    public enum DoctorType{
        Admin("Admin Organization"), Doctor("Doctor Organization"), Nurse("Nurse Organization"), OrganAdmin("OrganAdmin"),NGO ("NGO Organization");
        private String value;
        private DoctorType(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }
    
     public enum ORType{
        ORAdmin("Admin Organization");
        private String value;
        private ORType(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }
     
     public enum NGO{
        NGOAdmin("Admin Organization");
        private String value;
        private NGO(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }

    public Organization(String name) {
        this.name = name;
        workQueue = new WorkQueue();
        employeeDirectory = new EmployeeDirectory();
        userAccountDirectory = new UserAccountDirectory();
        fundRequestQueue = new FundRequestQueue();
        organizationID = counter;
        donorWorkQueue=new DonorWorkQueue();
        ++counter;
    }

    public DonorWorkQueue getDonorWorkQueue() {
        return donorWorkQueue;
    }

    public void setDonorWorkQueue(DonorWorkQueue donorWorkQueue) {
        this.donorWorkQueue = donorWorkQueue;
    }

    
    public FundRequestQueue getFundRequestQueue() {
        return fundRequestQueue;
    }

    public void setFundRequestQueue(FundRequestQueue fundRequestQueue) {
        this.fundRequestQueue = fundRequestQueue;
    }

    public abstract ArrayList<Role> getSupportedRole();
    
    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public int getOrganizationID() {
        return organizationID;
    }

    public EmployeeDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }
    
    public String getName() {
        return name;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }

    @Override
    public String toString() {
        return name;
    }
    
    
}
