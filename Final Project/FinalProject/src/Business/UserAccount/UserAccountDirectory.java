/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.UserAccount;

import Business.EcoSystem;
import Business.Employee.Employee;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Role.Role;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author raunak
 */
public class UserAccountDirectory {

    private ArrayList<UserAccount> userAccountList;

    public UserAccountDirectory() {
        userAccountList = new ArrayList<>();
    }

    public ArrayList<UserAccount> getUserAccountList() {
        return userAccountList;
    }

    public UserAccount authenticateUser(String username, String password) {
        for (UserAccount ua : userAccountList) {
            if (ua.getUsername().equals(username) && ua.getPassword().equals(password)) {
                return ua;
            }
        }
        return null;
    }

    public boolean nameExists(String name,EcoSystem system)
    {
            for (Network network : system.getNetworkList()) {
            for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                for (UserAccount emp : enterprise.getUserAccountDirectory().getUserAccountList()){
                if (name.equalsIgnoreCase(emp.getUsername())) {
                JOptionPane.showMessageDialog(null, "The name already exists, Please enter a different name");
                return false;
                }
              }
            }
    
        } 
        return true;
    }
    public boolean UserNameExists(String uname) {
        for (UserAccount ua : userAccountList) {
            if (uname.equalsIgnoreCase(ua.getUsername())) {
                JOptionPane.showMessageDialog(null, "The User name already exists, Please enter a different user name");
                return false;
            }
        }
        return true;
    }

    public UserAccount createUserAccount(String username, String password, Employee employee, Role role, EcoSystem eco) {

        for (Network net : eco.getNetworkList()) {
            System.out.println(net.getName());
            for (Enterprise ent : net.getEnterpriseDirectory().getEnterpriseList()) {
                System.out.println(ent.getName());
                for (UserAccount ua : ent.getUserAccountDirectory().getUserAccountList()) {
                    System.out.println(ua.getUsername());
                    if (ua.getUsername().equalsIgnoreCase(username)) {
                        return null;
                    }
                }
            }
        }
        UserAccount userAccount = new UserAccount();
        userAccount.setUsername(username);
        userAccount.setPassword(password);
        userAccount.setEmployee(employee);
        userAccount.setRole(role);
        userAccountList.add(userAccount);
        return userAccount;
    }

    public boolean checkIfUsernameIsUnique(String username) {
        for (UserAccount ua : userAccountList) {
            if (ua.getUsername().equals(username)) {
                JOptionPane.showMessageDialog(null, "The User name already exists, Please enter a different User Name");
                return false;
            }
        }
        return true;
    }

    public boolean ifNameIsUnique(String name) {
        for (UserAccount ua : userAccountList) {
            if (ua.getEmployee().getName().equals(name)) {
                return false;
            }
        }
        return true;
    }

    public void deleteUserAccount(String name) {
        System.out.println("name : " + name);
        for (UserAccount userAccount : userAccountList) {
            System.out.println("name : " + name + "userAccount : " + userAccount.getUsername());
            if (userAccount.getUsername().equals(name)) {
                userAccountList.remove(userAccount);
            }
        }

    }
}
