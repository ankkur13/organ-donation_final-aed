/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.PatientDetails;

import java.util.ArrayList;

/**
 *
 * @author shreya
 */
public class PatientDirectory {
    
      private ArrayList<Patient> patientList;

    public PatientDirectory() {
        patientList = new ArrayList<>();
    }

    public ArrayList<Patient> getPatientList() {
        return patientList;
    }

   
    
    public Patient createPatient(){
        Patient patient = new Patient();
       patientList.add(patient);
        return patient;
    }
    
    public void deleteEmployee(Patient patient)
    {
        patientList.remove(patient);
    }
    
    public Patient fetchPatientByName(String Name)
    {
         for (Patient patient : patientList){
            if (patient.getName().equals(Name))
                return patient;
        }
        return null;
    }
    
}
