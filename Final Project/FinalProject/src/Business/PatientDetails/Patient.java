/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.PatientDetails;

/**
 *
 * @author shreya
 */
public class Patient {

    private String name;
    private int age;
    private String gender;
    private String occupation;
    private int annualIncome;
    private String transplant;
    private String insurance;
    private int insurancePercent;
    private int fundRequirement;

    public Patient() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public int getAnnualIncome() {
        return annualIncome;
    }

    public void setAnnualIncome(int annualIncome) {
        this.annualIncome = annualIncome;
    }

    public String getTransplant() {
        return transplant;
    }

    public void setTransplant(String transplant) {
        this.transplant = transplant;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public int getInsurancePercent() {
        return insurancePercent;
    }

    public void setInsurancePercent(int insurancePercent) {
        this.insurancePercent = insurancePercent;
    }

    public int getFundRequirement() {
        return fundRequirement;
    }

    public void setFundRequirement(int fundRequirement) {
        this.fundRequirement = fundRequirement;
    }
    
    public String toString()
    {
        return this.name;
    }

}
