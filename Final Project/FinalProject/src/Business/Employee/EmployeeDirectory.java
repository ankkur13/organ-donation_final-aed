/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Employee;

import Business.Enterprise.Enterprise;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author raunak
 */
public class EmployeeDirectory {

    private ArrayList<Employee> employeeList;

    public EmployeeDirectory() {
        employeeList = new ArrayList<>();
    }

    public ArrayList<Employee> getEmployeeList() {
        return employeeList;
    }

    public boolean nameExists(String name) {
        for (Employee emp : employeeList) {
            if (name.equalsIgnoreCase(emp.getName())) {
                JOptionPane.showMessageDialog(null, "The name already exists, Please enter a different employee");
                return false;
            }
        }
        return true;
    }

    public Employee createEmployee(String name) {
        Employee employee = new Employee();
        employee.setName(name);
        employeeList.add(employee);
        return employee;
    }

    public void deleteEmployee(Employee employee) {
        employeeList.remove(employee);
    }
}
