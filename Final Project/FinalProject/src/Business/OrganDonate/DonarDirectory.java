/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.OrganDonate;

import java.util.ArrayList;

/**
 *
 * @author shreya
 */
public class DonarDirectory {
    private ArrayList<Donor> donorList;

    public DonarDirectory() {
        donorList = new ArrayList<>();
    }

    public ArrayList<Donor> getDonorList() {
        return donorList;
    }

    

   
    
    public Donor createPatient(){
        Donor donor = new Donor();
       donorList.add(donor);
        return donor;
    }
    
    public void deleteEmployee(Donor donor)
    {
        donorList.remove(donor);
    }
    
    public Donor fetchPatientByName(String Name)
    {
         for (Donor donor : donorList){
            if (donor.getDonorName().equals(Name))
                return donor;
        }
        return null;
    }
    
    
}
