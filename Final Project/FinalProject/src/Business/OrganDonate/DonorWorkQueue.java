/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.OrganDonate;

import Business.WorkQueue.*;
import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public class DonorWorkQueue {
    
    private ArrayList<DonorWorkRequest> donorWorkRequestList;

    public DonorWorkQueue() {
        donorWorkRequestList = new ArrayList<>();
    
    }

    public ArrayList<DonorWorkRequest> getDonorWorkRequestList() {
        return donorWorkRequestList;
    }

   
    public void addWorkRequest(DonorWorkRequest wr){
        donorWorkRequestList.add(wr);
    }
}