/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.OrganDonate;

/**
 *
 * @author JAI JINENDRA
 */
public class Donor {

    public String phoneNo;
     public String isNgo;
      public String ngoName;

    public String getIsNgo() {
        return isNgo;
    }

    public void setIsNgo(String isNgo) {
        this.isNgo = isNgo;
    }

    public String getNgoName() {
        return ngoName;
    }

    public void setNgoName(String ngoName) {
        this.ngoName = ngoName;
    }

      
    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public void setDonorName(String donorName) {
        this.donorName = donorName;
    }
    public String donorName;

    public String getDonorName() {
        return donorName;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

}
