/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.NGO;

import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author amandeep
 */
public class VolunteerDirectory {
     private ArrayList  <Volunteer> volunteerList;
    
    public VolunteerDirectory(){
        volunteerList = new ArrayList();
    }

    public ArrayList<Volunteer> getVolunteerList() {
        return volunteerList;
    
    }
    public Volunteer addVolunteer(){
            Volunteer volunteer = new Volunteer();
            volunteerList.add (volunteer);
            return volunteer;
}
    public void removeVolunteer(Volunteer val) {
        volunteerList.remove(val);
}
    public boolean idExists(int id1 ) {
        for (Volunteer v : volunteerList) {
            if (id1 == Integer.parseInt(v.getVolunteerId())) {
                JOptionPane.showMessageDialog(null, "The id already exists, Please enter a different id");
                return false;
            }
        }
        return true;
    }
}
