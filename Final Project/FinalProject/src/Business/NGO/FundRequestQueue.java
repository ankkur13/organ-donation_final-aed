/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.NGO;

import java.util.ArrayList;

/**
 *
 * @author amandeep
 */
public class FundRequestQueue {
    
    private ArrayList<FundWorkRequest> fundrequestList;
    
    public FundRequestQueue(){
        fundrequestList = new ArrayList<>();
    }

    public ArrayList<FundWorkRequest> getFundrequestList() {
        return fundrequestList;
    }
    public FundWorkRequest addFundRequest(){
        FundWorkRequest fr=new FundWorkRequest();
        fundrequestList.add(fr);
        return fr;
        
    }

   
}
