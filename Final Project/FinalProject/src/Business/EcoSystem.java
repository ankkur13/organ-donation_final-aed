package Business;

import Business.Employee.Employee;
import Business.Enterprise.Enterprise;
import Business.NGO.VolunteerDirectory;
import Business.Network.Network;
import Business.Organization.Organization;
import Business.Role.Role;
import Business.Role.SystemAdminRole;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.WorkQueue;
import java.util.ArrayList;
import Business.Organ.OrganCatalogue;
import Business.NGO.FundDonorDirectory;
import Business.PatientDetails.PatientDirectory;
import javax.swing.JOptionPane;

/**
 *
 * @author Administrator
 */
public class EcoSystem extends Organization {

    private static EcoSystem business;
    private ArrayList<Network> networkList;
    private WorkQueue workQueue;
    private OrganCatalogue organCatalogue;
    private VolunteerDirectory volunteerDirectory;
    private FundDonorDirectory fundDonorDirectory;
    private PatientDirectory patientDirectory;
    
    
    private EcoSystem() {
        super(null);
        networkList = new ArrayList<>();
        workQueue = new WorkQueue();
        organCatalogue = new OrganCatalogue();
        volunteerDirectory=new VolunteerDirectory();
        fundDonorDirectory=new FundDonorDirectory();
        patientDirectory=new PatientDirectory();
    }

    public PatientDirectory getPatientDirectory() {
        return patientDirectory;
    }

    public void setPatientDirectory(PatientDirectory patientDirectory) {
        this.patientDirectory = patientDirectory;
    }
    
    
    public VolunteerDirectory getVolunteerDirectory() {
        return volunteerDirectory;
    }

    public void setVolunteerDirectory(VolunteerDirectory volunteerDirectory) {
        this.volunteerDirectory = volunteerDirectory;
    }

    

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }

    public static EcoSystem getInstance() {
        if (business == null) {
            business = new EcoSystem();
        }
        return business;
    }

    

    public FundDonorDirectory getFundDonorDirectory() {
        return fundDonorDirectory;
    }

    public void setFundDonorDirectory(FundDonorDirectory fundDonorDirectory) {
        this.fundDonorDirectory = fundDonorDirectory;
    }

    public ArrayList<Network> getNetworkList() {
        return networkList;
    }

    public Network createAndAddNetwork() {
        Network network = new Network();
        networkList.add(network);
        return network;
    }
    
    public boolean networkExists(String network) {
        for (Network nw : networkList) {
            if (network.equalsIgnoreCase(nw.getName())) {
                JOptionPane.showMessageDialog(null, "The name already exists, Please enter a different network");
                return false;
            }
        }
        return true;
    }
    
    
    

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roleList = new ArrayList<>();
        roleList.add(new SystemAdminRole());
        return roleList;
    }

    public boolean checkIfUsernameIsUnique(String username) {

        if (!this.getUserAccountDirectory().checkIfUsernameIsUnique(username)) {
            return false;
        }

        return true;
    }

    public Enterprise getEnterprise(UserAccount uc) {
        for (Network network : networkList) {
            for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                    for (UserAccount ua : organization.getUserAccountDirectory().getUserAccountList()) {
                        if (ua == uc) {
                            return enterprise;
                        }
                    }
                }
            }
        }
        return null;
    }

    public Network getNetwork(UserAccount uc) {
        for (Network network : networkList) {
            for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                    for (UserAccount ua : organization.getUserAccountDirectory().getUserAccountList()) {
                        if (ua == uc) {
                            return network;
                        }
                    }
                }
            }
        }
        return null;
    }

    public Organization getOrganization(Employee e) {
        for (Network network : networkList) {
            for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                    for (Employee emp : organization.getEmployeeDirectory().getEmployeeList()) {
                        if (emp == e) {
                            return organization;
                        }
                    }
                }
            }
        }
        return null;
    }

    public Network getNetworkByName(String networkName) {

        for (Network network : networkList) {
            if (network.getName().equals(networkName)) {
                return network;
            }
        }
        return null;
    }

    public OrganCatalogue getOrganCatalogue() {
        return organCatalogue;
    }

    public void setOrganCatalogue(OrganCatalogue organCatalogue) {
        this.organCatalogue = organCatalogue;
    }

    public void deleteNetowrk(Network network) {
        networkList.remove(network);
    }

}
